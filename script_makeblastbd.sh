#!/bin/bash

#stocker le chemin du dossier qui contient les bases de données de sequences (BDSeq) dans une variable
BDSeq=/home/ctatout/WORK/ESSAI                 

#Aller chercher les fichiers DB
SAMP=$(find $BDSeq -name "*.fa" -type f)       

#stocker chaque nom de fichier BDD dans la variable $i en faisant une boucle (@ une après l'autre)
for i in ${SAMP[@]} ;                          
do

#Créer une variable qui contiendra le nom de fichier de résultat pour chaque BDD
F1=$(basename $BDSeq/$i).txt                   

#Lancer la commande blast sur les base de données de chaque espèce
makeblastdb -in $i  -dbtype prot -parse_seqids -out $F1 -blastdb_version 5
done 
